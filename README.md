GKE-packer-ansible-traefik-gitlab
=================================

Objective:
----------
 - Deploy refinerycms app for the development evironment.
 - Do it with ansible for deployment, packer for docker image refinerycms creation,
   gke for high availability docker service orchestration, traefik as a loadbalancer,
   and finale gitlab as executor first executor of all of this.

How:
----
 - Use of gitlab as public runners to execute ansible and packer.
 - Use of gke as public container service.
 - Packer as a tool for self-created images without complexity.
 - Ansible for play the roles of gke deployments.
 - traefik as gke ingress HA for kubernetes.


Technical annotations:
----------------------
 - I think that ansible and packer can be replaced by gitlab-ci.
 - There's no actual gke deployed, but imo it should be the easy part for only one cluster.
 - 
